import CTextField from './components/CTextField'
import CSelectBox from './components/CSelectBox'

export function install (Vue) {
    Vue.component('CTextField', CTextField)
    /* -- Add more components here -- */
  }

  export {
      CTextField,
      CSelectBox
  }

  const plugin = {
    /* eslint-disable no-undef */
    install,
  }
  